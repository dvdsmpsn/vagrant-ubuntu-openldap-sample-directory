# How to build an OpenLDAP instance on a new Ubuntu VM

## Introduction

### Prerequisites

1. [VirtualBox](https://www.virtualbox.org/) - a server virtualisation product
2. [Vagrant](http://vagrantup.com/) - 'Development environments made easy.'
	- Install with: `gem install vagrant`

## This directory contains

1. **Vagrantfile** is a main [Vagrant](http://vagrantup.com/) configuration. Additional documentation can be found [here](http://docs.vagrantup.com/v2/vagrantfile/index.html).
2. **samples/** a bunch of scripts and ldif to populate the openldap
3. Some images

## To finalize the setup and run your box you should:

 - In terminal, cd into this directory e.g. `cd ~/VMs/vagrant-ubuntu-openldap-sample-directory/vagrant/`
 - Run `vagrant up` to download, provision and start your brand new box environment.
 - Run `vagrant ssh` to get onto your guest system
 - Run `sh /home/vagrant/samples/setup-openldap.sh` on the guest system (or run each command sequentially) -- N.B. use `password` as the LDAP admin's password when prompted

 
## To test the setup, search for all entries:

Vagrant is setup to forward the LDAP port (389) on the guest machine to port 1389 on the host machine. This means that the OpenLDAP instance can be accessed on localhost

On the host machine (using port forwarding 389 -> 1389):

	ldapsearch -h 127.0.0.1 -p 1389 -D cn=admin,dc=example,dc=com -w password -b "dc=example,dc=com" -s sub "(objectclass=*)"
	
If this fails...	

On the guest machine (accessing port 389 directly):	
	
	ldapsearch -h 127.0.0.1 -p 389 -D cn=admin,dc=example,dc=com -w password -b "dc=example,dc=com" -s sub "(objectclass=*)"
	
## To use with your Confluence development environment

1. Add a new User Directory in *Confluence Admin | Users & Security | User Directories*:
	
	- **Name:** `Local OpenLDAP server`
	- **Directory Type:** `OpenLDAP`
	- **Hostname** `localhost`
	- **Port:** `1389`
	- **Username:** `cn=admin,dc=example,dc=com`
	- **Password:** `password`
	- **Base DN:** `dc=example,dc=com`
	- Expand the **User Schema Settings** section and change **User Name Attribute** to `uid`
		- This allows the standard Confluence hover cards to work correctly in the Enterprise Directory, otherwise they will fail
	
	<img src="vagrant/images/Confluence%20OpenLDAP%20setup.png?raw" width="100%">
	
2. Configure Enterprise Directory to use the OpenLDAP directory:
	<img src="vagrant/images/Enterprise%20Directory%20setup.png?raw" width="100%">

	
## Sample data

### English Royal Family

This LDAP contains a subset of the English Royal Family is available in this LDAP.  For the pedants, the data uses the format `Own Forename` `Wife's surname` for a little variety.

### Reloading data

If you wish to alter and reload this data:

1. Edit `/home/vagrant/samples/royal.example.com.ldif`
2. Run `/home/vagrant/samples/reload-royals.sh` to delete all the Royal Family members and reload them from the original `royal.example.com.ldif` file

## Testing

### Enterprise directory

{{todo}}

### Org Chart testing

To see a good example org chart, try `{org-chart:username=victoriaSCG}` -- *in old wiki markup terms* :)

- you should see something like this:

	<img src="vagrant/images/org-chart-example.png?raw" width="100%">




